import { Component} from '@angular/core';

@Component({
    selector: 'my-app',
    template: `<buttons-top></buttons-top><router-outlet></router-outlet><form-comp></form-comp>`,
    styleUrls: ['./app.component.css']
})
export class AppComponent {
}
