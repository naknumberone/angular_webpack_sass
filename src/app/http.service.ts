import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class HttpService{
    constructor(private http: HttpClient){}
    postMsg(message: string){
        console.log(message);
        this.http.post('http://angular.test/processingForm.php', message, {responseType: 'text'})
            .subscribe(data=>console.log("DATA: "+data), error=>console.log("ERROR: "+error));
    }
}