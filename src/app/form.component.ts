import { Component } from '@angular/core';
import { HttpService } from './http.service';

export class Msg{
    constructor(public name:string, public mail:string,
         public phone:string, public msg:string){}
}

@Component({
    selector: '<form-comp></form-comp>',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.css'],
    providers: [HttpService]
})
export class Form{
    private message: Msg;
    constructor(private httpService: HttpService){}
    creatMsg(name:string, mail:string,
        phone:string, msg:string){
            this.message = new Msg(name, mail, phone, msg);
            console.log(this.message); 
            this.httpService.postMsg(JSON.stringify(this.message)); 
    }
}

