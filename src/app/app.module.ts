import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';
import {AppComponent} from './app.component';
import {ButtonsTop} from './buttonsTop.component';
import {Form} from './form.component';
import {Canvas} from './canvas.component';
import { HttpClientModule } from '@angular/common/http';

const appRoutes:Routes = [
    { path: 'area/:id', component: Canvas }
];

@NgModule({
    imports:[BrowserModule, FormsModule, HttpClientModule, RouterModule.forRoot(appRoutes)],
    declarations: [AppComponent, ButtonsTop, Form, Canvas],
    bootstrap: [AppComponent]
})
export class AppModule{}
