import { Component } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';

@Component({
    selector: 'canvas-comp',
    template: '<canvas></canvas>',
    styleUrls: ['./canvas.component.css']
})
export class Canvas{
    id: number;
    private subscription: Subscription;
    constructor(private activateRoute: ActivatedRoute){
        this.subscription = activateRoute.params.subscribe(params=>{this.id=params['id'], 
            console.log("THIS ID: "+this.id);});
    }
}